const express = require("express");
const usersControllers = require("../controllers/usersControllers.js");
const auth = require("../auth.js")

const router = express.Router();

// Routes

// route for registration
router.post("/register", usersControllers.registerUser);

// route for login
router.post("/login", usersControllers.loginUser);

// Activity
router.get("/details", auth.verify, usersControllers.getProfile);

// tokenVerification
// router.get("/verify", auth.verify);

// route for course enrollment
router.post("/enroll", auth.verify, usersControllers.enrollCourse)

router.get("/userDetails", auth.verify, usersControllers.retrieveUserDetails);

module.exports = router;