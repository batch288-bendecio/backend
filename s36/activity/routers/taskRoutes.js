const express = require("express");
const taskControllers = require("../controllers/taskControllers.js");

// Contain all the endpoints of our application
const router = express.Router();

router.get("/", taskControllers.getAllTasks);

router.post("/addtask", taskControllers.addTasks);

// Parameterizer

// We are going to create a route using a Delete method at the URL "/task/:id"
// The colon here is an identifier that helps to create a dynamic route which allows us to supply information

router.delete("/:id", taskControllers.deleteTask)

router.get("/:id", taskControllers.getSpecificTask)

router.put("/:id/completed", taskControllers.updateTaskStatus)


module.exports = router;