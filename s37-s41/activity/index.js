const express = require("express");
const mongoose = require("mongoose");


// It will allow our backend application to be available to our frontend application
// It will also allows us to control the app's Cross Origin Resource Sharing settings.
const cors = require("cors")

const usersRoutes = require("./routes/usersRoutes.js")

const coursesRoutes = require("./routes/coursesRoutes.js")

const port = 4001;

const app = express();

// MongoDB connection
// Establish the connection between the DB and the application or server.
// Thename of the server should be "courseBookingAPI"
mongoose.connect("mongodb+srv://admin:admin@batch288bendecio.3fb8drl.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


const db = mongoose.connection;

db.on("error", console.error.bind(`Error! cannot connect to the cloud database`))
db.once("open", () => console.log("Connected to cloud database!"))


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}))

// Remminder that we are going to use this for the sake of the bootcamp
app.use(cors());

// Add the routing of the routes from the usersRoutes
app.use("/users", usersRoutes);
app.use("/courses", coursesRoutes);





if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}

module.exports = {app,mongoose};