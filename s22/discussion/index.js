// console.log("Good Afternoon, Batch 288!");

// Array Methods
	// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	// Mutator Methods
	// Mutator methods are function that "mutate" or change an array after they're created
	// These methods manipulate the original array performing various such as adding or removing elements.
	
	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

	// push()
		/*
			- Adds an element in the end of an array and returns the updated array's length
			Syntax:
				arrayName.push();
		*/

	console.log("Current array: ");
	console.log(fruits);

	let fruitsLength = fruits.push('Mango');

	console.log(fruitsLength);
	console.log('Mutated array from push method: ')
	console.log(fruits);



	fruitsLength = fruits.push('Avocado','Guava')
	console.log(fruitsLength)
	console.log("Mutated array after pushing multiple elements: ");
	console.log(fruits);

	// pop()
	/*
		-removes the last element and returns the removed element.
		Syntax:
			arrayName.pop();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	let removedFruits = fruits.pop();
	console.log(removedFruits);

	console.log("Mutated Array from the pop method:");
	console.log(fruits);

	// unshift()
	/*
		- it adds one or more elements at the beginning of an array and returns the updated array's length
		-Syntax:
			arrayName.unshift('elementA');
			arrayNAme.unshift('elementA', 'elementB', . . .)
	*/

	console.log("Current Array:");
	console.log(fruits);

	fruitsLength = fruits.unshift('Lime', 'Banana');

	console.log(fruitsLength);
	console.log("Mutated array from unshift method:")
	console.log(fruits);

	// shift()
	/*
		- removes an element at the beginning of an array and returns the removed element.
		Syntax:
			arrayName.shift();
	*/

	console.log("Current Array:");
	console.log(fruits);

	removedFruits = fruits.shift();

	console.log(removedFruits);
	console.log("Mutated array from the shift method:")
	console.log(fruits);

	// splice()
	/*
		- simultaneously removes an elements from a specified index number and adds element
		Syntax:
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
	*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.splice(4, 2, "Lime", "Cherry");
	console.log("Mutated array after the splice method: ");
	console.log(fruits);

	// sort()
		/*
			Rearranges the array elements in alphanumeric order
				Syntax:
				arrayName.sort();
		*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.sort();

	console.log("Mutated array from the sort method: ");
	console.log(fruits);

	// reverse()
		/*
			Reverses the order of array elements
			Syntax:
				arrayName.reverse();

		*/

	console.log("Current Array: ");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array from the sort method: ");
	console.log(fruits);

// [Section] Non-mutator Methods
	/*
		- Non-mutator methods are functions that do not modify or change an array after the're created.
		-These methods do not manipulate the original array performing various task such as returning elements from an array and combining arrays and printing the output.
	*/

	let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

	// indexOF()
	// returns the index number of the first matching element found in an array.
	// If no match was found, the result will be -1.
	// The search process will be done from the first element proceeding to the last element.
	/*
		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue,startingIndex);
	*/

	let firstIndex = countries.indexOf('PH');
	console.log(firstIndex);

	let invalidCountry = countries.indexOf('BR');
	console.log(invalidCountry);

	firstIndex = countries.indexOf('PH', 2);
	console.log(firstIndex);

	console.log(countries);

	// lastIndexOf()
	/*
		- returns the index number of the last matching element found in an array
		- the search process will be done from last element proceeding to the first element

			Syntax:
				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue,startingIndex);
	
	*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log(lastIndex);

	invalidCountry = countries.lastIndexOf('BR');
	console.log(invalidCountry);

	lastIndexOf = countries.lastIndexOf('PH',4);
	console.log(lastIndexOf);

	// slice()
		/*
			- portion/slices elements from an array and return a new array
			Syntax:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
		*/
	// Slicing off element from a specified index to the last element

	let slicedArrayA = countries.slice(2);
	console.log("Result from slice method: ");
	console.log(slicedArrayA);

	// Slicing of element from a specified index to another index:
	// The elements that will be sliced are elements from the starting index until the element before the ending index.
	let slicedArrayB = countries.slice(2,4);
	console.log("Result from slice method: ");
	console.log(slicedArrayB);

	// slicing off elements starting from the last element of an array:

	let slicedArrayC = countries.slice(-3);
	console.log("Result from slice method: ");
	console.log(slicedArrayC);

	// toString()
	/*
		returns an array as string separated by commas syntax:
		arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log("Result from string method: ");
	console.log(stringArray);
	console.log(typeof stringArray);

	// concat()
	// combines arrays to an array or elements and returns the combined result.
	/*
		Syntax:
			arrayA.concat(arrayB);
			arrayA.concat(elementA);
	*/

	let tasksArrayA = ["drink HTML", "eat JavaScript"];
	let tasksArrayB = ["inhale CSS", "breath SASS"];
	let tasksArrayC = ["get git","be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	let combinedTasks = tasksArrayA.concat("smell express","throw react");
	console.log(combinedTasks);

	// concat multiple array into an array

	let allTasks = tasksArrayA.concat(tasksArrayB,tasksArrayC);
	console.log(allTasks);

	// concat array to an array and element
	let exampleTasks = tasksArrayA.concat(tasksArrayB,"smell express");
	console.log(exampleTasks);

	// join()
	// returns an array as string separated by specified separator string
	/*
		Syntax:
			arrayName.join("separatorString")
	*/

	let users = ['John','Jane','Joe','Robert'];

	console.log(users.join());
	console.log(users.join(''));
	console.log(users.join(" - "));

// [Section] Iteration Methods
	// Iteration methods are loops designed to perform repititive task
	// Iteration methods loops over all items in an array

	// forEach()
	// Similar to a for loop that iterates on each of array element.
	/*
		Syntax:
			arrayName.forEach(function(indivElement){statement};)
	*/

	console.log(allTasks);
	// ['drink HTML', 'eat JavaScript', 'inhale CSS', 'breath SASS', 'get git', 'be node']

	allTasks.forEach(function(task){
		console.log(task);
	});

	// filteredTask variable will hold all the elements from the allTasks array that has more than 10 characters.
	let filteredTasks = [];

	allTasks.forEach(function(task){
		if(task.length > 10){
			filteredTasks.push(task);
		}
	})

	console.log(filteredTasks);

	// map()
	// Iterates on each element and returns new array with different values depending on the resut of the function's operation

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function(number){
		return number + 1;
	})
	console.log(numbers);
	console.log(numberMap);

	// every()
	/*	
		it will check if all elements in an array meet the given condition.
		-return true value if all elements meet the condition and false otherwise.

		Syntax:
			let/const resultArray = arrayName.every(function(indivElement){
				return expression/condition;
			})
	*/

	numbers = [1, 2, 3, 4, 5];

	let allValid = numbers.every(function(number){
		return (number < 6);
	})

	console.log(allValid);

	// some()
	/*
		check if atleast one element in the array meets the given condition.

		Syntax: 
			let/const resultArray = arrayName.some(function(indivElement){
				retur expression/condition;
			})
	*/

	let someValid = numbers.some(function(number){
		return (number < 2);
	})

	console.log(someValid);

	// filter()
	/*
		returns new array that contains element which meets the given condition
		-returns an empty array if no elements were found

		Syntax:
			let/const resultArray = arrayName.filter(function(indivElement){
				return expression/condition;
			})
	*/

	numbers = [1, 2, 3, 4, 5];

	let filterValid = numbers.filter(function(number){
		return (number % 2 === 0);
	})

	console.log(filterValid);

	// includes()
	/*
		checks if the argument passed can be found in the array
	*/

	let products = ['Mouse','Keyboard','Laptop','Monitor'];

	let productFound1 = products.includes('Mouse');
	console.log(productFound1);

	// reduce()
	/*
		evaluates element from left to right and returns/reduces the array into single value
	*/
	// The first parameter in the function will be accumulator
	// The second parameter in the function will be currentValue
	let reducedArray = numbers.reduce(function(x, y){
		console.log('Accumulator: ' + x);
		console.log('currentValue: ' + y);
		return x * y;
	})

	console.log(reducedArray);