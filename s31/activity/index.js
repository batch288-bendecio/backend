const http = require("http");

const port = 3000;

const server = http.createServer((request, response) => {

	if (request.url == "/login"){
		response.writeHead(200, {"Content-type" : "text/plain"})

		response.end("You are in the login page!")
	} else {
		response.writeHead(404, {"Content-type" : "text/plain"})
		response.end("Page is not available")
	}
})

server.listen(port)

console.log(`Server is successfully running at localhost:${port}.`);