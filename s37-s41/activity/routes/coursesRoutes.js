const express = require("express");
const coursesControllers = require("../controllers/coursesControllers.js");
const auth = require("../auth.js")

const router = express.Router();

// Without params

// This route is responsible for adding course in our db.

router.post("/addCourse", auth.verify, coursesControllers.addCourse);

// Route for retrieving all courses

router.get("/", auth.verify, coursesControllers.getAllCourses);

// Route for retrieving all active courses
router.get("/activeCourses", coursesControllers.getActiveCourses);

// route for inactive courses
router.get("/inactiveCourses", auth.verify, coursesControllers.getInactiveCourses);

// With params

// Route for getting the specific course information

router.get("/:courseId", coursesControllers.getCourse)

// route for updating course
router.patch("/:courseId", auth.verify, coursesControllers.updateCourse);

// route for archiving a course
router.patch("/:courseId/archive", auth.verify, coursesControllers.archiveCourse)




module.exports = router;