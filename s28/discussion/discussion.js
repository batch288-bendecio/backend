// show databases - to see the list of databases
// use "dbname" to use specifiec db
// show collections to see the list of all the collections inside our db

// CRUD Operation
/*
	- CRUD operation is the heart of any backend application.
	- mastering the CRUD operation is essential for any developer especially to those who want to become backend developer.
*/

// [Section] Inserting Document (Create)
	// Insert one document
		/*
			Since mongoDB deals with objects as it's structure for documents we can easily create them by providing objects in our method/operation.

			Syntax: 
				db.collectionName.insertOne({
					object
				})
		*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

// Insert Many

/*
	Syntax:
		db.collectionName.insertMany([{objectA},{objectB}]);
*/

db.userss.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: { 
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses:  ["Python", "React", "PHP"],
			department: "none"
		},

		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React","Laravel","Sass"],
			department: "none"
		}
	])

// [Section] Finding documents (Read Operation)
	/*
		Syntax 1:
			db.collectionName.find();

		Syntax 2:
			db.collectionName.find({field:value})
	*/

// Using the find() method, it will show you the list of all the documents inside our collection.
db.users.find();

// The "pretty" methods allows us to be able to view the documents returned by or terminals to be in better format.
db.users.find().pretty();

// it will return the documents that will pass the criteria given in the method
db.users.find({ firstName : "Stephen" });

db.users.find({_id : ObjectId("646c56526b255b9e4dff381b")})

// multiple criteria
db.users.find({lastName: "Armstrong", age:82});
db.users.find({"contact.phone" : "123456789"});

// [Section] Updating documents(update)

db.users.insertOne({
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

// updateOne method
/*
	Syntax:
		db.collectionName.updateOne({criteria,{$set: {field:value}}});
*/

db.users.updateOne(
		{firstName : "Bill"},{
			$set: {
				firstName: "Chris"
			}
		}
	)

db.users.updateOne(
	{ firstName : "Jane" },
	{
		$set: {
			lastName: "Edited"
		}
	}
	)

// Updating Multiple Documents
/*
	Syntax:
		db.collectionName.updateMany(
			{criteria},
			{
				$set: {
					{field:value}
				}
			}
		)
*/

db.users.updateMany(
		{ department : "none" },
		{
			$set: {
				department: "HR"
			}
		}
	)

// Replace One
	/*
		Syntax: db.collectionName.replaceOne(
			{criteria},
			{
				object
			}
		)
	*/

db.users.insertOne({firstName: "test"});

db.users.replaceOne(
		{firstName: "test"},
		{
			firstName: "Bill",
			lastName: "Gates",
			age:65,
			contact:{},
			courses:[],
			department: "Operations"
			
		}
	)

// [Section] Deleting Documents

// Deleting Single Documents

	/*
		db.collectionName.deleteOne({criteria})
	*/

db.users.deleteOne({firstName: "Bill"});

// Deleting Multiple document
	/*
		db.collectionName.deleteMany({criteria})
	*/

db.users.deleteMany({firstName: "Jane"});

// All the documents in our collection will be deleted
db.users.deleteMany({});